var path = require("path")
var express = require("express")
var logger = require("morgan")
var bodyParser = require("body-parser") // simplifies access to request body

var app = express()  // make express app
var http = require('http').Server(app)  // inject app into the server
// 1 set up the view engine
app.set("views", path.resolve(__dirname, "views")) // path to views
app.set("view engine", "ejs") // specify our view engine

app.use(express.static(__dirname +'/assets'))
//app.use(express.static(href="/guestbook.css"));
// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))
// 4 handle http GET requests (default & /new-entry)
app.get("/", function (request, response) {
  response.render("Sherpa_Pemba");
});

app.get("/Sherpa_Pemba", function (request, response) {
  response.render("Sherpa_Pemba");
});

app.get("/about", function (request, response) {
  response.render("Sherpa_Pemba");
});
app.get("/choice", function(request,response){
  response.render("Choice");
});

app.get("/contact", function (request, response) {
  response.render("contact");
});

app.get("/guestbook", function (request, response) {
  response.render("index");
});


app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

// 5 handle an http POST request to the new-entry URI 
app.post("/new-entry", function (request, response) {
  console.log(request.body);
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({  // store it
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");  // where to go next? Let's go to the home page :)
});

app.post("/contact", function (request, response) {
  var api_key = 'key-d20fb27a91a44b043f432cc74067b8d9';
  var domain = 'sandbox513f24e47fae46aab770d8f712d4926e.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: '<postmaster@sandbox513f24e47fae46aab770d8f712d4926e.mailgun.org>',
    to: 's525998@nwmissouri.edu',
    subject: request.body.email,
    text: request.body.message
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    if(!error)
      response.send("Mail sent!");
    else
      response.send("Mail not sent!");
  });
});

app.use(function (request, response) {
  response.status(404).render("404");
  });

// Listen for an application request on port 8081
http.listen(8081, function () {
  console.log('Guestbook app listening on http://127.0.0.1:8081/')
})
